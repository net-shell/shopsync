# ShopSync

## Introduction

ShopSync is an e-commerce service that allows clients to sell their products on multiple websites, all in one place.
Products can be imported from third-party sources and then published on different channels, such as eBay, Amazon or Microweber website.

This MVP covers the free tier of the application. Premium features are planned for development after initial pitch.

Domain: http://shop-sync.com/
