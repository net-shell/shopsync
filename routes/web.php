<?php

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', function () {
    if(\Auth::check()) return redirect('home');
    return view('welcome');
});

Route::resource('products', 'ProductController')->middleware('auth');
Route::resource('channels', 'ChannelController')->middleware('auth');

Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
