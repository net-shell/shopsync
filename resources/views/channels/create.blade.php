@extends('layouts.app')

@section('title', 'Create Channel')

@section('body')

<form class="form" action="{{ url('/channels') }}" method="POST">
    {{ csrf_field() }}
    <div class="form-group">
        <button class="btn btn-primary" type="submit">Save</button>
    </div>
    <div class="form-group">
        <select class="form-control" name="driver">
            <option value="ebay">eBay</option>
            <option value="amazon">Amazon</option>
            <option value="microweber">Microweber</option>
        </select>
    </div>
</form>

@endsection