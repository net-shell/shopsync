@extends('layouts.app')

@section('title', 'Channels')

@section('body')

<div class="well">
    <a class="btn btn-success" href="{{ url('/channels/create') }}">
        <i class="fa fa-plus"></i>
        Create
    </a>
</div>

<table class="table table-hover table-alternating">
    <thead>
        <tr>
            <th>Driver</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($channels as $channel)
        <tr>
            <td>{{ $channel->driver }}</td>
            <td class="text-right">
                <form action="{{ url('/channel/' . $channel->id) }}" method="POST">
                    <input type="hidden" name="_method" value="DELETE">
                    {{ csrf_field() }}
                    <input class="btn btn-danger btn-xs" type="submit" value="Delete">
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection