@extends('layouts.app')

@section('title', $product->name)

@section('body')

<form class="form form-horizontal" action="{{ url('/products/' . $product->id) }}" method="POST">
    {{ csrf_field() }}
    <input name="_method" type="hidden" value="PUT">
    <input name="id" type="hidden" value="{{ $product->id }}">
    <div class="row">
        <div class="col-sm-8">
            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-check"></i>
                        Save
                    </button>
                    <a class="btn btn-default" href="{{ url('/products') }}">
                        Cancel
                    </a>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8">
                    <input class="form-control" name="name" placeholder="Name" value="{{ $product->name }}">
                </div>
                <div class="col-sm-4">
                    <input class="form-control" name="price" placeholder="Price" value="{{ $product->price }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <textarea class="form-control" name="description" rows="6" placeholder="Description">{{ $product->description }}</textarea>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <div class="col-sm-12">
                    <h4>Sync on channels</h4>
                    @foreach($channels as $channel)
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="channels[]" value="{{ $channel->id }}" {{ in_array($channel->id, $productChannels) ? 'checked' : '' }}>
                            {{ $channel->driver }}
                        </label>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</form>

@endsection