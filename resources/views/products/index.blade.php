@extends('layouts.app')

@section('title', 'Products')

@section('body')

<div class="well">
    <a class="btn btn-success" href="{{ url('/products/create') }}">
        <i class="fa fa-plus"></i>
        Create
    </a>
    <div class="dropdown" style="display: inline-block;">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-plus"></i>
            Add From
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu">
            @foreach($channels as $channel)
            <li><a href="#">{{ $channel->driver }}</a></li>
            @endforeach
            <li><a href="{{ url('/channels/create') }}">Create new channel</a></li>
        </ul>
    </div>
</div>

<table class="table table-hover table-alternating">
    <thead>
        <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Category</th>
            <th>Channels</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($products as $product)
        <tr>
            <td>
                <a href="{{ url('/products/' . $product->id) }}">{{ $product->name }}</a>
            </td>
            <td>{{ $product->price }}</td>
            <td></td>
            <td>
                @foreach($product->channels as $channel)
                <span class="label label-primary">{{ $channel->driver }}</span>
                @endforeach
            </td>
            <td class="text-right">
                <form action="{{ url('/products/' . $product->id) }}" method="POST">
                    <input type="hidden" name="_method" value="DELETE">
                    {{ csrf_field() }}
                    <input class="btn btn-danger btn-xs" type="submit" value="Delete">
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection