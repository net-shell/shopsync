@extends('layouts.app')

@section('title', 'Create Products')

@section('body')

<form class="form form-horizontal" action="{{ url('/products') }}" method="POST">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-sm-8">
            <div class="form-group">
                <div class="col-sm-12">
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-check"></i>            
                        Save
                    </button>
                    <a class="btn btn-default" href="{{ url('/products') }}">
                        Cancel
                    </a>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8">
                    <input class="form-control" name="name" placeholder="Name">
                </div>
                <div class="col-sm-4">
                    <input class="form-control" name="price" placeholder="Price">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <textarea class="form-control" name="description" rows="6" placeholder="Description"></textarea>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="form-group">
                <div class="col-sm-12">
                    <h4>Sync on channels</h4>
                    @foreach($channels as $channel)
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="channels[]" value="{{ $channel->id }}">
                            {{ $channel->driver }}
                        </label>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</form>

@endsection