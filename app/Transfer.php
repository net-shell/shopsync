<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    protected $fillable = ['channel_id', 'product_id', 'is_complete'];

    public function channel() {
        return $this->belongsTo('App\\Channel');
    }
}
