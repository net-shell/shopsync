<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class Product extends Model
{
    use HasApiTokens;

    protected $fillable = ['name', 'price', 'description', 'user_id'];

    public function user() {
        return $this->belongsTo('App\\User');
    }

    public function transfers() {
        return $this->hasMany('App\\Transfer');
    }

    public function channels() {
        return $this->belongsToMany('App\\Channel');
    }
}
