<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $fillable = ['driver', 'credentials', 'user_id'];

    public function user() {
        return $this->belongsTo('App\\User');
    }

    public function transfers() {
        return $this->hasMany('App\\Transfer');
    }

    public function products() {
        return $this->belongsToMany('App\\Product');
    }
}
